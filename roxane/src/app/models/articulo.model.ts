export class articulo {
    private selected: boolean;
    public servicios:string[];

    constructor(public nombre: string, public u: string){
        this.servicios=['desayuno','piscina','wifi'];
    }

    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
}
