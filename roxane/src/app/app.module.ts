import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PersonalizacionComponent } from './personalizacion/personalizacion.component';
import { CatalogoHombreComponent } from './catalogo-hombre/catalogo-hombre.component';
import { ArticuloDetalleComponent } from './articulo-detalle/articulo-detalle.component';

const routes: Routes = [
  { path:'', redirectTo: 'home', pathMatch: 'full' },
  { path:'home', component: CatalogoHombreComponent },
  { path:'articulo', component: ArticuloDetalleComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    PersonalizacionComponent,
    CatalogoHombreComponent,
    ArticuloDetalleComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule, 
    ReactiveFormsModule, 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
