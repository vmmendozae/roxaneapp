import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { articulo } from '../models/articulo.model';

@Component({
  selector: 'app-personalizacion',
  templateUrl: './personalizacion.component.html',
  styleUrls: ['./personalizacion.component.css']
})
export class PersonalizacionComponent implements OnInit {
  @Input() articuloCatalogo: articulo;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<articulo>;

  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.articuloCatalogo);
    return false; 
  }

}
