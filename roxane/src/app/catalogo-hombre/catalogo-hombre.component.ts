import { Component, OnInit } from '@angular/core';
import { articulo } from '../models/articulo.model';

@Component({
  selector: 'app-catalogo-hombre',
  templateUrl: './catalogo-hombre.component.html',
  styleUrls: ['./catalogo-hombre.component.css']
})
export class CatalogoHombreComponent implements OnInit {
  articulos: articulo[];
  constructor() { 
    this.articulos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    this.articulos.push(new articulo(nombre, url));
    return false;
  }

  elegido(d: articulo){
    this.articulos.forEach(function (x) {x.setSelected(false); });
    d.setSelected(true);
  }

}
