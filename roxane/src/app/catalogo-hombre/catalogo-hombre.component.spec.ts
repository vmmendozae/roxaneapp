import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoHombreComponent } from './catalogo-hombre.component';

describe('CatalogoHombreComponent', () => {
  let component: CatalogoHombreComponent;
  let fixture: ComponentFixture<CatalogoHombreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoHombreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoHombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
